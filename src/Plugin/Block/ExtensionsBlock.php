<?php

/**
 * @file
 * Contains \Drupal\extensions_block\Plugin\Block\ExtensionsBlock
 */

namespace Drupal\extensions_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block\Annotation\Block;
use Drupal\Core\Annotation\Translation;

/**
 * Provides an 'Extensions' block.
 *
 * @Block (
 *  id = "extensions_block",
 *  admin_label = @Translation("List of installed extensions"),
 *  module = "extensions_block"
 *  )
 */
class ExtensionsBlock extends BlockBase {
  public function build() {
    $list = \Drupal::service("extension.list.module")->getList();
    $extensions = [];
    $extensions[] = '<b>drupal:</b> ' . $list['system']->info["version"];
    foreach ($list as $e) {
      $name = $e->getName();
      $version = empty($e->info["version"]) ? '<i>custom</i>' : $e->info["version"];
      if ($e->origin != 'core') {
        $extensions[] = '<b>' . $name . ':</b> ' . $version;
      }
    }

    // Wrap each item in '#markup' so that the tags are
    // rendered, not escaped.
    $list_item_markup = [];
    foreach ($extensions as $item) {
      $list_item_markup[] = [
        '#markup' => $item,
      ];
    }

    return array(
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list_item_markup,
    );
  }
}
