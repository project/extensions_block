CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Motivation
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

Extensions Block is a simple module that displays all of the installed extensions in a configurable block. To use it, enable the module at Admin > Extend and move it to any region you want at Admin > Structure > Blocks > Place Blocks.


MOTIVATION
----------

Any page that contains the Extensions Block will always fail a Visual Regression Test done after an upgrade. This module is therefore useful in various types of VRT testing.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Extensions Block module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

No configuration is needed.
